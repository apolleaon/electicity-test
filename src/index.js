const checkPlant = (el) => el.getStatus();

class Household {
  constructor() {
    this.powerPlants = [];
    this.households = [];
  }

  connectToHouseHold(household) {
    this.households.push(household);
  }

  connectToPowerPlant(powerPlant) {
    this.powerPlants.push(powerPlant);
  }

  disconnectPowerPlant(powerPlant) {
    this.powerPlants = this.powerPlants.filter((plant) => plant !== powerPlant);
  }

  hasElectricity(household) {
    let plantsStatus = this.powerPlants.some(checkPlant);
    let housholdsStatus = () => {
      for (let house of this.households) {
        if (house !== household && house.hasElectricity(this)) return true;
        else return false;
      }
      return false;
    };
    return plantsStatus || housholdsStatus();
  }
}

class PowerPlant {
  constructor() {
    this.isAlive = true;
  }

  kill() {
    this.isAlive = false;
  }

  repair() {
    this.isAlive = true;
  }

  getStatus() {
    return this.isAlive;
  }
}

export class World {
  constructor() {}

  createPowerPlant() {
    return new PowerPlant();
  }

  createHousehold() {
    return new Household();
  }

  connectHouseholdToPowerPlant(household, powerPlant) {
    household.connectToPowerPlant(powerPlant);
  }

  connectHouseholdToHousehold(household1, household2) {
    household1.connectToHouseHold(household2);
    household2.connectToHouseHold(household1);
  }

  disconnectHouseholdFromPowerPlant(household, powerPlant) {
    household.disconnectPowerPlant(powerPlant);
  }

  killPowerPlant(powerPlant) {
    powerPlant.kill();
  }

  repairPowerPlant(powerPlant) {
    powerPlant.repair();
  }

  householdHasEletricity(household) {
    return household.hasElectricity();
  }
}
